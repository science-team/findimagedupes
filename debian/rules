#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk
pkg=findimagedupes
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

%:
	dh $@

execute_before_dh_auto_configure:
	# Autogenerate the C code from inline in findimagedupes.
	cp debian/C.pm.stub C.pm
	sed -n '/^__C__$$/,$$p' findimagedupes >> C.pm
	# Inject Debian invented Makefile.PL
	cp debian/Makefile.PL.stub Makefile.PL
	# Prepare upstream version number, but from Debian changelog;
	# see the "dist" target from upstream Makefile (and #1023939).
	echo $(DEB_VERSION) | cut -f 1 -d - > VERSION
	./patchver

override_dh_auto_build:
	# Bug #531145: Removing installation instructions from the manpage, as
	# they are useless if the package is installed. Upstream has no other
	# documentation, so this section will stay in upstream's version.
	if grep -q '^=head1 INSTALLATION' $(pkg); then \
        sed -i.pristine '/^=head1 INSTALLATION/,/^=head1 /{/^=head1 OPTIONS/p;d;}' \
		$(pkg); fi
	# Upstream has now updated README himself in version 2.18
	# It might be reasonable to create the README this way anyway
	# pod2text findimagedupes > ./tmp/README
	dh_auto_build

override_dh_clean:
	[ ! -f findimagedupes.pristine ] || mv findimagedupes.pristine findimagedupes
	rm -f C.pm Makefile.PL
	dh_clean

override_dh_auto_clean:
